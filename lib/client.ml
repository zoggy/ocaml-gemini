open Types

type error =
| Invalid_uri of Uri.t * string
| Bad_response of string

let string_of_error = function
| Invalid_uri (uri, msg) ->
    Printf.sprintf "Invalid uri %s: %s" (Uri.to_string uri) msg
| Bad_response msg ->
    Printf.sprintf "Bad server response: %s" msg

exception Error of error
let () = Printexc.register_printer
  (function Error e -> Some (string_of_error e) | _ -> None)

let error e = raise (Error e)
let lwt_error e = Lwt.fail (Error e)

let mk_status code meta =
  match code / 10 with
  | 1 -> `Input { sensitive = code mod 10 = 1 ; prompt = meta }
  | 2 ->
      (
       match Ldp.Ct.of_string meta with
       | Ok mime -> `Success mime
       | Error e ->
           error (Bad_response
            (Printf.sprintf "%s: %s" meta (Ldp.Ct.string_of_error e)))
      )
  | 3 ->
    let uri =
      try Uri.of_string meta
      with _ -> error (Bad_response (Printf.sprintf "Invalid uri: %s" meta))
    in
    `Redirect { permanent = code mod 10 = 1; uri }
  | 4 ->
      let (k, msg) = match code mod 10 with
        | 1 -> `Server_unavailable, meta
        | 2 -> `Cgi_error, meta
        | 3 -> `Proxy_error, meta
        | 4 ->
            (try `Slow_down (int_of_string meta), ""
             with _ ->
                 error (Bad_response (Printf.sprintf "Invalid slow down delay: %s" meta))
            )
        | _ -> `Other, meta
      in
      `Temp_failure (k, msg)
  | 5 ->
      let k =
        match code mod 10 with
        | 1 -> `Not_found
        | 2 -> `Gone
        | 3 -> `Proxy_request_refused
        | 4 -> `Bad_request
        | _ -> `Other
      in
      `Perm_failure (k, meta)
  | 6 ->
      let k =
      match code mod 10 with
        | 1 -> `Cert_required
        | 2 -> `Cert_not_authorised
        | 3 -> `Cert_not_valid
        | _ -> `Other
      in
      `Cert_required (k, meta)
  | _ ->
      error (Bad_response (Printf.sprintf "Invalid status code: %d" code))

let read_body ct ic =
  match ct.Ldp.Ct.ty with
  | Ldp.Ct.Text -> (* read line by line to normalize newlines *)
      let stream = Lwt_io.read_lines ic in
      let b = Buffer.create 256 in
      let rec iter () =
        match%lwt Lwt_stream.get stream with
        | None -> Lwt.return_unit
        | Some line ->
            Buffer.add_string b line; Buffer.add_char b '\n';
            iter ()
      in
      let%lwt () = iter () in
      Lwt.return (Buffer.contents b)
  | _ -> Lwt_io.read ic

let read_until_crlf =
  let rec iter b ic =
    match%lwt Lwt_io.read_char_opt ic with
    | None -> lwt_error (Bad_response "Missing <CR><LF>")
    | Some c when c = '\r' ->
        begin
          match%lwt Lwt_io.read_char_opt ic with
          | Some c when c = '\n' ->
              Lwt.return (Buffer.contents b)
          | _ -> lwt_error (Bad_response "Missing <LF>")
        end
    | Some c ->
        Buffer.add_char b c;
        iter b ic
  in
  fun ic -> iter (Buffer.create 1024) ic

let read_response ic =
  try%lwt
    let%lwt line = read_until_crlf ic in
    let len = String.length line in
    let%lwt code =
      if len < 3 then
        lwt_error (Bad_response (Printf.sprintf "Response too short: %S" line))
      else
        if String.get line 2 <> ' ' then
          lwt_error (Bad_response "Missing space")
        else
          let s = String.sub line 0 2 in
          try Lwt.return (int_of_string s)
          with _ ->
              lwt_error (Bad_response (Printf.sprintf "Bad code: %s" s))
    in
    let meta = String.sub line 3 (len-3) in
    let status = mk_status code meta in
    match status with
    | `Success mime ->
        let%lwt body = read_body mime ic in
        Lwt.return { status ; body }
    | _ -> Lwt.return { status ; body = "" }
  with
  | End_of_file -> lwt_error (Bad_response "Missing header data")


let query q =
  let%lwt authenticator = Lwt.return (fun ?ip ~host certs -> Ok None) in
  let%lwt host = match Uri.host q with
    | None -> lwt_error (Invalid_uri (q, "Missing host"))
    | Some h -> Lwt.return h
  in
  let port = match Uri.port q with
    | None -> 1965
    | Some p -> p
  in
  let%lwt (ic,oc) = Tls_lwt.connect authenticator (host,port) in
  try%lwt
   let%lwt () = Lwt_io.write oc (Printf.sprintf "%s\r\n" (Uri.to_string q)) in
   let%lwt () = Lwt_io.flush oc in
   let%lwt r = read_response ic in
   Lwt_io.close ic ;
   Lwt_io.close oc ;
   Lwt.return r
  with
  | e ->
    Lwt_io.close ic;
    Lwt_io.close oc;
    Lwt.fail e
