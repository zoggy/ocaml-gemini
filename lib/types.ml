type query = Uri.t

type input = {
  prompt: string ;
  sensitive: bool ;
  }

type redirect = {
  permanent: bool;
  uri: Uri.t ;
  }

type temp_failure = [
  | `Other
  | `Server_unavailable
  | `Cgi_error
  | `Proxy_error
  | `Slow_down of int (** number of seconds to wait before sending request again *)
]
type permanent_failure = [
  | `Other
  | `Not_found
  | `Gone
  | `Proxy_request_refused
  | `Bad_request
]

type client_cert_required = [
  | `Other
  | `Cert_required
  | `Cert_not_authorised
  | `Cert_not_valid
]

type status =
[ `Input of input
| `Success of Ldp.Ct.t
| `Redirect of redirect
| `Temp_failure of temp_failure * string
| `Perm_failure of permanent_failure * string
| `Cert_required of client_cert_required * string
]

let code_string_of_status = function
| `Input { prompt ; sensitive } ->
    Printf.sprintf "1%d %s" (if sensitive then 1 else 0) prompt
| `Success ct ->
    Printf.sprintf "20 %s" (Ldp.Ct.to_string ct)
| `Redirect { permanent ; uri } ->
    Printf.sprintf "3%d %s" (if permanent then 1 else 0) (Uri.to_string uri)
| `Temp_failure (t, msg) ->
    let code, msg =
      match t with
      | `Other -> 0, msg
      | `Server_unavailable -> 1, msg
      | `Cgi_error -> 2, msg
      | `Proxy_error -> 3, msg
      | `Slow_down n -> 4, string_of_int n
    in
    Printf.sprintf "4%d %s" code msg
| `Perm_failure (t, msg) ->
    Printf.sprintf "5%d %s"
      (match t with
       | `Other -> 0
       | `Not_found -> 1
       | `Gone -> 2
       | `Proxy_request_refused -> 3
       | `Bad_request -> 4
      ) msg
| `Cert_required (t, msg) ->
    Printf.sprintf "6%d %s"
      (match t with
       | `Other -> 0
       | `Cert_required -> 1
       | `Cert_not_authorised -> 2
       | `Cert_not_valid -> 3
      )
      msg

let http_code_of_status = function
| `Input _ -> 403
| `Success _ -> 200
| `Redirect r -> if r.permanent then 308 else 307
| `Temp_failure (t, _) ->
    (match t with
    | `Other -> 500
    | `Server_unavailable -> 503
    | `Cgi_error -> 500
    | `Proxy_error -> 502
    | `Slow_down n -> 429
    )
| `Perm_failure (t, _) ->
    (match t with
     | `Other -> 500
     | `Not_found -> 404
     | `Gone -> 410
     | `Proxy_request_refused -> 502
     | `Bad_request -> 400
    )
| `Cert_required (t, _) -> 403

type response_body = string

type response =
  { status: status ;
    body: response_body ;
  }

let default_ct =
  match Ldp.Ct.of_string "text/gemini; charset=utf-8" with
  | Ok t -> t
  | Error _ -> assert false

let mime_gemini = Ldp.Ct.to_mime default_ct
