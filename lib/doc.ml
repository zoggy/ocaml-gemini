
type line =
| Text of string
| Link of string * string option
| Pre of string * string
| Header of int * string (* first level is 1 *)
| List of string list
| Quote of string

type t = line list

let htab = [%sedlex.regexp? 0x09] (* horizontal tab *)
let sp = [%sedlex.regexp? ' '] (* space, \x20 *)
let wsp = [%sedlex.regexp? sp | htab] (* white space *)

let newline = [%sedlex.regexp? Opt('\r'),'\n']
let text = [%sedlex.regexp? Star(Compl(Chars "\n\r"))]

let link_start = [%sedlex.regexp? "=>",Star(wsp)]
let quote_start = [%sedlex.regexp? ">",text]
let pre = [%sedlex.regexp? "```"]
let header = [%sedlex.regexp? Plus('#')]
let li = [%sedlex.regexp? "* "]

(* tools to handle locations in lexbuf *)

let pos ?(file="") ~line ~bol ~char () =
  Lexing.{ pos_lnum = line ; pos_bol = bol ; pos_cnum = char ; pos_fname = file }

type loc = { loc_start: Lexing.position; loc_stop: Lexing.position }
type 'a with_loc = 'a * loc option

type error = loc * string
exception Error of error
let error ?(msg="Parse error") loc = raise (Error (loc, msg))

let string_of_loc loc =
  let open Lexing in
  let start = loc.loc_start in
  let stop = loc.loc_stop in
  let line = start.pos_lnum in
  let char = start.pos_cnum - start.pos_bol in
  let len =
    if start.pos_fname = stop.pos_fname then
      stop.pos_cnum - start.pos_cnum
    else
      1
  in
  let file = start.pos_fname in
  Printf.sprintf "%sline %d, character%s %d%s"
    (match file with
     | "" -> ""
     | _ -> Printf.sprintf "File %S, " file)
    line
    (if len > 1 then "s" else "")
    char
    (if len > 1 then Printf.sprintf "-%d" (char + len) else "")

let loc_sprintf loc fmt =
  match loc with
  | None -> Printf.sprintf fmt
  | Some loc -> Printf.ksprintf
      (fun s -> Printf.sprintf "%s:\n%s" (string_of_loc loc) s)
      fmt

let string_of_error (loc, str) =
  Printf.sprintf "%s: %s" (string_of_loc loc) str

let loc loc_start loc_stop = { loc_start ; loc_stop }
let loc_of_pos pos len =
  { loc_start = pos ;
    loc_stop = Lexing.{ pos with pos_cnum = pos.pos_cnum + len } ;
  }
let error_pos ?msg pos = error ?msg (loc_of_pos pos 1)

let nl_char = Uchar.of_char '\n'

let update_pos pos str =
  let open Lexing in
  let f pos i = function
  | `Malformed msg -> error ~msg (loc_of_pos pos 1)
  | `Uchar c when Uchar.equal c nl_char ->
      let bol = pos.pos_cnum in
      { pos with
        pos_lnum = pos.pos_lnum + 1;
        pos_bol = bol ;
        pos_cnum = pos.pos_cnum + 1 ;
      }
  | _ -> { pos with pos_cnum = pos.pos_cnum + 1}
  in
  Uutf.String.fold_utf_8 f pos str

let lexeme pos lexbuf =
  try Sedlexing.Utf8.lexeme lexbuf
  with Sedlexing.MalFormed ->
    error_pos ~msg:"Malformed character in lexeme" pos

let upd pos lexbuf = update_pos pos (lexeme pos lexbuf)

(* parsing *)

let textline pos lb =
  match%sedlex lb with
  | text ->
      let str = lexeme pos lb in
      let pos = upd pos lb in
      (str, pos)
  | eof -> assert false
  | _ -> ("", pos)

let rec newlines acc pos lb =
  match%sedlex lb with
  | newline ->
      let pos = upd pos lb in
      newlines (Text "" :: acc) pos lb
  | any ->
      Sedlexing.rollback lb;
      (acc, pos)
  | eof -> (acc, pos)
  | _ -> (acc, pos)

let rec pre_lines text b pos lb =
  match%sedlex lb with
  | pre ->
      let pos = upd pos lb in
      (Buffer.contents b, pos)
  | eof ->
      (Buffer.contents b, pos)
  | any ->
      let str = lexeme pos lb in
      let pos = upd pos lb in
      Buffer.add_string b str ;
      pre_lines text b pos lb
  | _ -> assert false

let rec pre ?(text="") pos lb =
  match%sedlex lb with
  | text ->
      let text = lexeme pos lb in
      let pos = upd pos lb in
      pre ~text pos lb
  | newline ->
      let pos = upd pos lb in
      let (str, pos) = pre_lines text (Buffer.create 256) pos lb in
      (text, str, pos)
  | eof -> (text, "", pos)
  | _ -> assert false

let rec link_text uri pos lb =
  match%sedlex lb with
  | newline ->
      Sedlexing.rollback lb;
      (uri, None, pos)
  | wsp ->
      let pos = upd pos lb in
      link_text uri pos lb
  | any ->
      Sedlexing.rollback lb ;
      let (t, pos) = textline pos lb in
      (uri, Some t, pos)
  | eof -> (uri, None, pos)
  | _ -> assert false

let link pos lb =
  match%sedlex lb with
  | Plus(Compl(Chars " \t\n\r")) ->
      let uri = lexeme pos lb in
      let pos = upd pos lb in
      link_text uri pos lb
  | _ ->
      let pos = upd pos lb in
      ("", None, pos)

let headtext pos lb =
  match%sedlex lb with
  | text ->
      let pos = upd pos lb in
      let str = lexeme pos lb in
      (str, pos)
  | _ -> assert false

let rec head pos lb =
  match%sedlex lb with
  | wsp ->
      head (upd pos lb) lb
  | any ->
      Sedlexing.rollback lb;
      headtext pos lb
  | _ -> ("", pos)

let rec list acc pos lb =
  match%sedlex lb with
  | Star(newline), li, text ->
      let pos = upd pos lb in
      let s = lexeme pos lb in
      let p = String.index_from s 0 '*' in
      let text = String.sub s (p + 2) (String.length s - p - 2) in
      list (text :: acc) pos lb
  | eof -> (List.rev acc, pos)
  | any ->
      Sedlexing.rollback lb;
      (List.rev acc, pos)
  | _ -> (List.rev acc, pos)

let rec doc acc pos lb =
  match%sedlex lb with
  | newline ->
      let (acc, pos) = newlines acc pos lb in
      doc acc pos lb
  | li ->
      Sedlexing.rollback lb;
      let (items, pos) = list [] pos lb in
      doc (List items :: acc) pos lb
  | header ->
      let pos = upd pos lb in
      let level = String.length (lexeme pos lb) in
      let (text, pos) = head pos lb in
      doc (Header (level, text) :: acc) pos lb
  | pre ->
      let pos = upd pos lb in
      let (intro, block, pos) = pre pos lb in
      doc (Pre (intro, block) :: acc) pos lb
  | link_start ->
      let pos = upd pos lb in
      let (uri, text, pos) = link pos lb in
      doc (Link (uri, text) :: acc) pos lb
  | quote_start, text ->
      let lexeme = lexeme pos lb in
      let pos = upd pos lb in
      let t = String.sub lexeme 1 (String.length lexeme - 1) in
      doc (Quote t :: acc) pos lb
  | any ->
      Sedlexing.rollback lb;
      let (t, pos) = textline pos lb in
      doc (Text t :: acc) pos lb
  | eof -> List.rev acc
  | _ -> List.rev acc

let of_string str =
  let pos = pos ~line:0 ~bol:0 ~char:0 () in
  let lexbuf = Sedlexing.Utf8.from_string str in
  doc [] pos lexbuf

let to_string =
  let f b = function
  | Text text -> Printf.bprintf b "%s\n" text
  | Pre (intro,block) ->
     Printf.bprintf b "```%s\n%s```\n" intro block
  | Link (uri,text) ->
     Printf.bprintf b "=> %s%s\n" uri
       (match text with
         | None -> ""
         | Some s -> " "^s)
  | Quote text ->
     Printf.bprintf b ">%s\n" text
  | Header (level, text) ->
     Printf.bprintf b "%s %s\n" (String.make level '#') text
  | List items ->
     List.iter (fun s -> Printf.bprintf b "* %s\n" s) items;
  in
  fun doc ->
    let b = Buffer.create 256 in
    List.iter (f b) doc;
    Buffer.contents b
